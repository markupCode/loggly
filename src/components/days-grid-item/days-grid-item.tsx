import React from "react";
import styled from "styled-components";

const Div = styled.div`
  height: 1em;
  width: 1em;

  border-radius: 0.08em;
  border-style: solid;
  border-color: transparent;

  background-color: ${props => props.color};
`;

export type HistoryDayOverviewProps = {
  day: Date;
  color: string;
};

export const HistoryDayOveriew: React.FunctionComponent<HistoryDayOverviewProps> = ({
  color
}) => {
  return <Div color={color}></Div>;
};
