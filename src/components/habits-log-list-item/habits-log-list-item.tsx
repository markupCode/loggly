import React, { FunctionComponent } from "react";

import { Container } from "@material-ui/core";

import { Habit } from "../../core/models/habit";
import { HabitHistoryEntry } from "../../core/models/habit-history-entry";

export type HabitLogListItemProps = {
  habit: Habit;
  history: HabitHistoryEntry;
};

export const HabitsLogListItem: FunctionComponent<HabitLogListItemProps> = ({
  habit,
  history
}) => {
  return (
    <Container>
      <h4>{habit.name}</h4> <p>{history.date.toString()}</p>{" "}
      <p>#{history.action}</p>{" "}
    </Container>
  );
};
