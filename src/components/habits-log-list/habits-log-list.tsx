import React, { FunctionComponent, Fragment, createContext } from "react";

import { Habit } from "../../core/models/habit";
import { HabitHistoryEntry } from "../../core/models/habit-history-entry";
import { List } from "@material-ui/core";

export type HabitsLogListProps = {
  habitsLogs: Array<{ habit: Habit; history: HabitHistoryEntry }>;
};

const { Provider, Consumer } = createContext({
  habit: {
    id: 0,
    name: "",
    history: new Array<HabitHistoryEntry>()
  },
  history: { date: new Date(), action: "" }
});

export const HabitsLogListItemConsumer = Consumer;

export const HabitsLogList: FunctionComponent<HabitsLogListProps> = ({
  habitsLogs,
  children
}) => {
  return (
    <List>
      {habitsLogs.map(log => (
        <Fragment key={log.history.date.getTime()}>
          {<Provider value={log}>{children}</Provider>}
        </Fragment>
      ))}
    </List>
  );
};
