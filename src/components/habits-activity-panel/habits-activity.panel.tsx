import React, { FunctionComponent } from "react";
import styled from "styled-components";

import { Container } from "@material-ui/core";

import { DaysGridContainer } from "../../containers/days-grid/days-grid.container";
import { HabitsLogListContainer } from "../../containers/habits-log-list/habits-log-list.container";
import { HabitsLogListItemConsumer } from "../habits-log-list/habits-log-list";
import { HabitsLogListItem } from "../habits-log-list-item/habits-log-list-item";

const ActiveDaysContainer = styled.div`
  padding: 0.5em;
  background-color: #eee;
`;

export const HabitsActivityPanel: FunctionComponent = () => {
  const ContextualHabitLogListItem = () => {
    return (
      <HabitsLogListItemConsumer>
        {props => (
          <HabitsLogListItem
            habit={props.habit}
            history={props.history}
          ></HabitsLogListItem>
        )}
      </HabitsLogListItemConsumer>
    );
  };

  return (
    <div>
      <ActiveDaysContainer>
        <Container maxWidth="sm">
          <DaysGridContainer></DaysGridContainer>
        </Container>
      </ActiveDaysContainer>
      <Container maxWidth="sm">
        <HabitsLogListContainer>
          <ContextualHabitLogListItem></ContextualHabitLogListItem>
        </HabitsLogListContainer>
      </Container>
    </div>
  );
};
