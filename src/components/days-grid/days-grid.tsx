import React from "react";

import { Grid } from "@material-ui/core";

import { HistoryDayOveriew } from "../days-grid-item/days-grid-item";

import { Habit } from "../../core/models/habit";
import { HistoryDays } from "../../core/services/habits.service";

import styled from "styled-components";

const PaddedContent = styled.div`
  padding: 0.1em;
`;

export type DaysGridProps = {
  historyDays: HistoryDays;
  getDayColor: (habits: Habit[]) => string;
};

export class DaysGrid extends React.Component<DaysGridProps> {
  render() {
    return (
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="center"
      >
        {Array.from(this.props.historyDays.entries()).map(([day, habits]) => (
          <PaddedContent key={day}>
            <HistoryDayOveriew
              day={new Date(day)}
              color={this.props.getDayColor(habits)}
            ></HistoryDayOveriew>
          </PaddedContent>
        ))}
      </Grid>
    );
  }
}
