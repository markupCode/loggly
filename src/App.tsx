import React from "react";
import { Provider } from "react-redux";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import { HabitsStore } from "./core/state/habits.store";
import { HabitsActivityPanel } from "./components/habits-activity-panel/habits-activity.panel";

import "./App.css";

function App() {
  return (
    <Provider store={HabitsStore}>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={HabitsActivityPanel}></Route>
        </Switch>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
