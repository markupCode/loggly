import { connect } from "react-redux";

import { getHabitsLogs } from "../../core/services/habits-logs.service";
import { HabitsState } from "../../core/state/habits.reducer";

import { HabitsLogList } from "../../components/habits-log-list/habits-log-list";

const mapStateToProps = (state: HabitsState) => {
  return {
    habitsLogs: getHabitsLogs(state.habits)
  };
};

export const HabitsLogListContainer = connect(
  mapStateToProps
)(HabitsLogList);
