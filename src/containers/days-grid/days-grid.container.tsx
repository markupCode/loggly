import { connect } from "react-redux";

import {
  groupHabitsByDays,
  getHistoryWindow
} from "../../core/services/habits.service";
import { HabitsState } from "../../core/state/habits.reducer";

import { DaysGrid } from "../../components/days-grid/days-grid";

const mapStateToProps = (state: HabitsState) => {
  return {
    historyDays: groupHabitsByDays(
      [...getHistoryWindow(state.viewOptions.historyWindow)],
      state.habits
    )
  };
};

const mapStateToActions = () => {
  return {
    getDayColor: (habits: any[]) =>
      habits.length > 0 ? "lightgreen" : "lightgray"
  };
};

export const DaysGridContainer = connect(
  mapStateToProps,
  mapStateToActions
)(DaysGrid);
