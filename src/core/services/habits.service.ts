import { Habit } from "../models/habit";

export type HistoryDays = Map<number, Habit[]>;

export function getDateWithoutTime(date: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

export function groupHabitsByDays(days: Date[], habits: Habit[]): HistoryDays {
  const grouped: HistoryDays = new Map(days.map(day => [day.getTime(), []]));

  habits.forEach((habit: Habit) => {
    habit.history.forEach(day => {
      const date = getDateWithoutTime(day.date);

      if (grouped.has(date.getTime())) {
        return grouped.get(date.getTime())?.push(habit);
      }
    });
  });

  console.log(grouped);

  return grouped;
}

export function getHistoryWindow(windowType: number | "year" | "month") {
  const getDateRangeToNow = (window: number) => {
    return getDateRange(
      getDateWithoutTime(
        new Date(
          getDateWithoutTime(new Date()).getTime() -
            1000 * 60 * 60 * 24 * window
        )
      ),
      getDateWithoutTime(new Date())
    );
  };

  if (typeof windowType === "number") {
    return getDateRangeToNow(windowType);
  }

  return getDateRangeToNow(31);
}

function* getDateRange(fromDate: Date, toDate: Date) {
  let currentDate = fromDate;

  console.log(fromDate, toDate);

  while (currentDate < toDate) {
    currentDate = new Date(currentDate.getTime() + 1000 * 60 * 60 * 24);

    yield currentDate;
  }
}
