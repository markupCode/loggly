import { Habit } from "../models/habit";

export function getHabitsLogs(habits: Habit[]) {
  return habits
    .flatMap(habit =>
      habit.history.map(history => ({ habit, history: history }))
    )
    .sort(
      (one, another) =>
        one.history.date.getTime() - another.history.date.getTime()
    );
}
