import { HabitHistoryEntry } from "./habit-history-entry";

export interface Habit {
  id: number;
  name: string;
  history: HabitHistoryEntry[];
}
