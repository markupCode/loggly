export interface HabitHistoryEntry {
  date: Date;
  action: string;
}
