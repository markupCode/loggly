export enum RawHabitsActionTypes {
  "GET_ALL",
  "GET"
}

interface GetAllHabitsAction {
  type: RawHabitsActionTypes.GET_ALL;
}

interface GetHabitAction {
  type: RawHabitsActionTypes.GET;
  id: number;
}

export type HabitsActionTypes = GetAllHabitsAction | GetHabitAction;
