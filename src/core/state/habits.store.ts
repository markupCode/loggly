import { createStore } from "redux";

import { HabitsReducer } from "./habits.reducer";

export const HabitsStore = createStore(HabitsReducer);
