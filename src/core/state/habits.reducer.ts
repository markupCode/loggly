import { Habit } from "../models/habit";
import { HabitsActionTypes } from "./habits.action";

export type HabitsState = {
  habits: Habit[];
  viewOptions: {
    historyWindow: number | "month";
  };
};

const InitialState: HabitsState = {
  habits: [
    {
      id: 0,
      name: "Reading",
      history: [
        { date: new Date(2020, 2, 10), action: "completed" },
        { date: new Date(2020, 2, 24), action: "completed" },
        { date: new Date(2020, 2, 25), action: "completed" },
        { date: new Date(2020, 2, 26), action: "completed" }
      ]
    }
  ],
  viewOptions: {
    historyWindow: 90
  }
};

export function HabitsReducer(state = InitialState, action: HabitsActionTypes) {
  return state;
}
